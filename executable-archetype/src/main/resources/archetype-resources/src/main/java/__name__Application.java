package ${package};

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/** The entry point for the application. */
@SpringBootApplication
public class ${name}Application implements ApplicationRunner {

  /**
   * Launches this application.
   * 
   * @param args the command line arguments provided at runtime
   */
  public static void main(String[] args) {
    SpringApplication.run(${name}Application.class, args);
  }

  @Override
  public void run(ApplicationArguments args) {
    System.out.println("Hello World!");
  }
}
