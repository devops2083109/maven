package ${package};

import edu.byu.hbll.json.ObjectMapperFactory;
import edu.byu.hbll.json.UncheckedObjectMapper;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/** Spring bean configuration for the application. */
@Configuration
@AllArgsConstructor
public class ${name}Configuration {

  private ${name}Properties properties;

  @Bean
  public UncheckedObjectMapper objectMapper() {
    return ObjectMapperFactory.newUnchecked();
  }

  // TODO: Add configuration beans as needed.
}