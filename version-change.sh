#!/bin/bash
################################################################################
# Replaces references to the old version (first arg) with a new version
# (second arg) across this project.
################################################################################

THIS=`basename "$0"`
USAGE="Usage: $THIS OLD_VERSION NEW_VERSION"

if [ $# -lt 2 ]; then
  echo -e "$USAGE" >&2
  exit 1
fi

OLD_VERSION=$1
NEW_VERSION=$2

find -type f \
  -not -path "*.git/*" \
  -not -path "*target/*" \
  -print0 |\
  xargs -0 sed -i "s/$OLD_VERSION/$NEW_VERSION/g"
